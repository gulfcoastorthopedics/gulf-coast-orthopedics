Houma Orthopedic Clinic was founded in July 1975 for the purpose of providing quality orthopedic care within the Southeast Louisiana area. In 2016, Houma Orthopedic Clinic was renamed Gulf Coast Orthopedics comprised of offices in five locations in Southeastern Louisiana.

Address: 1001 School Street, Houma, LA 70360, USA

Phone: 985-868-1540

Website: [https://www.gulfcoastorthopedics.com](https://www.gulfcoastorthopedics.com)
